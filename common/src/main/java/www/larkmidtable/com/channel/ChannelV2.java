package www.larkmidtable.com.channel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import www.larkmidtable.com.element.Record;
import www.larkmidtable.com.reader.Reader;
import www.larkmidtable.com.writer.Writer;

import java.util.Queue;

/**
 * @Date: 2022/11/14 18:18
 * @Description:
 **/
public abstract class ChannelV2 {
    private static final Logger logger = LoggerFactory.getLogger(ChannelV2.class);

    private static Queue<Record> queue = null;

    public void setQueue(Queue<Record> queue) {
        ChannelV2.queue = queue;
    }

    public void channel(Reader reader, Writer writer, int pos, int count) {
        // 1.init 初始化
        reader.init(pos, count);
        writer.init();
        // 2.多线程并行读取
        reader.read(queue);
        // 3.多线程并行写入
        writer.write(queue);
        reader.stop();
        writer.stop();
    }
}
